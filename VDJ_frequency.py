#!/usr/bin/env python
"""
Generate frequency and Spearman rank correlation coefficient associated files
including Supplementary tables 1 and 2.
"""
__author__ = "Mark Chernyshev"

import os
import pandas as pd
import numpy as np
from scipy import stats
from tqdm import tqdm
from scipy import stats

macaque_collapsed_file = "../data/collected_macaques/collapsed_no_SHM.tab"
macaque_output_dir = "../data/collected_macaques/"

human_collapsed_file = "../data/collected_PRJEB26509_26_10_2021/collapsed_no_SHM.tab"
human_output_dir = "../data/collected_PRJEB26509_26_10_2021/"

def get_leader(string):
    """Given the name of a macaque library from Vazquez Bernat et al., return 
    True if generated with leader primers and False if 5' UTR primers"""
    r = int(string.split("_")[-1])
    if r < 100:
        return True
    else:
        return False


def VDJ_frequencies(clone_df, case_column, gene_column_suffix, extra_columns = []):
    """Generates collapsed and uncollapsed frequencies for a given VDJ column and case column.
    Columns in the extra_columns array are metadata on a certain case to be added to output."""
    cols = [case_column,  gene_column_suffix, 'frequency', 'uncollapsed_frequency', 'segment'] + extra_columns
    VDJ_freq_df = pd.DataFrame(columns = cols)
    for segment in tqdm(['V', "D", "J"]):
        for library, lib_df in clone_df.groupby(by = case_column):
            extra_columns_values = []
            for col in extra_columns:
                extra_columns_values = extra_columns_values +  [lib_df[col].values[0]]
            lib_size = len(lib_df)
            uncollapsed_lib_size = lib_df.uncollapsed_count.sum()
            for gene, gene_df in lib_df.groupby(by = segment + '_' + gene_column_suffix):
                freq = len(gene_df) / lib_size
                uncollapsed_freq = gene_df.uncollapsed_count.sum() / uncollapsed_lib_size
                temp_df = pd.DataFrame({case_column : library,
                                      gene_column_suffix : gene,
                                      'frequency' : freq,
                                      'uncollapsed_frequency' : uncollapsed_freq, 
                                      "segment" : segment}, index = [0])
                for i in range(len(extra_columns)):
                    temp_df[extra_columns[i]] = extra_columns_values[i]
                VDJ_freq_df = VDJ_freq_df.append(temp_df, ignore_index = True)
    VDJ_freq_df['log_frequency'] = np.log10(VDJ_freq_df['frequency'])
    return VDJ_freq_df.reset_index(drop = True)


clone_df = pd.read_csv(macaque_collapsed_file, sep = '\t')
clone_df['case'] = clone_df['case_y']
clone_df['library'] = clone_df['case_x']

for segment in ['V', 'D']:
    clone_df[segment + '_gene_family'] = clone_df[segment + '_gene'].apply(lambda x: x.split('-')[0])
    
#J genes do not have families, only genes
clone_df['J_gene_family'] = clone_df['J_gene']
clone_df['leader'] = clone_df['library'].apply(get_leader)

print("Calculating macaque VDJ gene frequencies per library")
VDJ_freq_per_lib_df = VDJ_frequencies(clone_df, "library", "gene", extra_columns = ["species", "subgroup", "leader"])
VDJ_freq_per_lib_df.to_csv(os.path.join(macaque_output_dir, 'VDJ_gene_frequency_per_library.tab'), sep = '\t', index = False)

print("Calculating macaque VDJ gene frequencies per case")
VDJ_freq_per_case_df = VDJ_frequencies(clone_df, "case", "gene",  extra_columns = ["species", "subgroup"])
VDJ_freq_per_case_df.to_csv(os.path.join(macaque_output_dir, 'VDJ_gene_frequency_per_case.tab'), sep = '\t', index = False)

expression_correlations = pd.DataFrame(columns = ['comparison', 'subgroup', 'gene', 'collapsed_spearman', 'uncollapsed_spearman'])
print("Calculating Spearman's rank correlation coefficients")
for segment in tqdm(['V', 'D', 'J']):
    gene_freqs_per_lib_df = VDJ_freq_per_lib_df[VDJ_freq_per_lib_df.segment.eq(segment)] 
    gene_freqs_per_case_df = VDJ_freq_per_case_df[VDJ_freq_per_case_df.segment.eq(segment)] 
    
    #leader vs 5' UTR per subgroup
    for subgroup, df in gene_freqs_per_lib_df.groupby(by = ['subgroup']):
        leader = df[df.leader.eq(1.0)]
        UTR = df[df.leader.eq(0.0)]
        if (len(leader) > 0) and (len(UTR) > 0):
            leader = leader.groupby(by = 'gene', as_index = False).mean()
            UTR = UTR.groupby(by = 'gene', as_index = False).mean()
            genes = list(set(leader['gene']).intersection(set(UTR['gene'])))
            leader = leader[leader['gene'].isin(genes)].sort_values(by = 'gene').reset_index(drop = True)
            UTR = UTR[UTR['gene'].isin(genes)].sort_values(by = 'gene').reset_index(drop = True)
            temp = pd.Series({'comparison' : "Leader vs 5' UTR", 
                                  'subgroup' : subgroup,
                                  'gene' : segment,
                                  'collapsed_spearman' : stats.spearmanr(leader.frequency.values, UTR.frequency.values)[0],
                                  'uncollapsed_spearman' : stats.spearmanr(leader.uncollapsed_frequency.values, UTR.uncollapsed_frequency.values).correlation, 'num_genes' : len(genes)})    
            expression_correlations = expression_correlations.append(temp, ignore_index = True)

    #leader vs 5' UTR per species
    for species, df in gene_freqs_per_lib_df.groupby(by = ['species']):
        if species == 'Rhesus':
            subgroups = ["Indian", "Chinese"]
        elif species == 'Cynomolgus':
            subgroups = ["Indonesian", "Mauritius"]
        leader = df[df.leader.eq(1.0)]
        UTR = df[df.leader.eq(0.0)]
        if (len(leader) > 0) and (len(UTR) > 0):
            leader = leader.groupby(by = 'gene', as_index = False).mean()
            UTR = UTR.groupby(by = 'gene', as_index = False).mean()
            genes = list(set(leader['gene']).intersection(set(UTR['gene'])))
            leader = leader[leader['gene'].isin(genes)].sort_values(by = 'gene').reset_index(drop = True)
            UTR = UTR[UTR['gene'].isin(genes)].sort_values(by = 'gene').reset_index(drop = True)
            temp = pd.Series({'comparison' : "Leader vs 5' UTR", 
                                  'subgroup' : " and ".join(subgroups),
                                  'gene' : segment,
                                  'collapsed_spearman' : stats.spearmanr(leader.frequency.values, UTR.frequency.values).correlation,
                                  'uncollapsed_spearman' : stats.spearmanr(leader.uncollapsed_frequency.values, UTR.uncollapsed_frequency.values).correlation, 'num_genes' : len(genes)})
            expression_correlations = expression_correlations.append(temp, ignore_index = True)

    #comparing subgroups
    for species, df in gene_freqs_per_case_df.groupby(by = ['species']):
        if species == 'Rhesus':
            subgroups = ["Indian", "Chinese"]
        elif species == 'Cynomolgus':
            subgroups = ["Indonesian", "Mauritius"]
        df1 = df[df.subgroup.eq(subgroups[0])]
        df2 = df[df.subgroup.eq(subgroups[1])]
        df1 = df1.groupby(by = 'gene', as_index = False).mean()
        df2 = df2.groupby(by = 'gene', as_index = False).mean()
        genes = list(set(df1['gene']).intersection(set(df2['gene'])))
        df1 = df1[df1['gene'].isin(genes)].sort_values(by = 'gene').reset_index(drop = True)
        df2 = df2[df2['gene'].isin(genes)].sort_values(by = 'gene').reset_index(drop = True)
        temp = pd.Series({'comparison' : 'Subgroup vs Subgroup', 
                              'subgroup' : subgroups[0] + " vs " + subgroups[1],
                              'gene' : segment,
                              'collapsed_spearman' : stats.spearmanr(df1.frequency.values, df2.frequency.values).correlation,
                              'uncollapsed_spearman' : stats.spearmanr(df1.uncollapsed_frequency.values, df2.uncollapsed_frequency.values).correlation, 'num_genes' : len(genes)})
        expression_correlations = expression_correlations.append(temp, ignore_index = True)

    #comparing species
    df = gene_freqs_per_case_df.groupby(by = ["species", "gene"], as_index = False).mean()
    c_df, r_df = df[df.species.eq("Cynomolgus")], df[df.species.eq("Rhesus")] 
    genes = set(c_df.gene.values).intersection(set(r_df.gene.values))
    c_df, r_df = c_df[c_df.gene.isin(genes)].sort_values(by = "gene").reset_index(drop = True), r_df[r_df.gene.isin(genes)].sort_values(by = "gene").reset_index(drop = True)
    temp = pd.Series({'comparison' : 'Rhesus vs Cynomolgus', 
                          'subgroup' : "all",
                          'gene' : segment,
                          'collapsed_spearman' : stats.spearmanr(c_df.frequency.values, r_df.frequency.values).correlation,
                          'uncollapsed_spearman' : stats.spearmanr(c_df.uncollapsed_frequency.values, r_df.uncollapsed_frequency.values).correlation,
                         'num_genes' : len(genes)})
    expression_correlations = expression_correlations.append(temp, ignore_index = True)
    
expression_correlations["collapsed_spearman"] = expression_correlations.collapsed_spearman.apply(lambda x: round(x, 3))
expression_correlations["uncollapsed_spearman"] = expression_correlations.uncollapsed_spearman.apply(lambda x: round(x, 3))
expression_correlations["num_genes"] = expression_correlations.num_genes.apply(lambda x: int(x))
expression_correlations.to_csv(os.path.join(macaque_output_dir, "expression_correlation_coefficients.tab"), sep = '\t', index= False)
expression_correlations.to_excel(os.path.join(macaque_output_dir, "Supplementary_table_2.xlsx"), index= False)

# make Supplementary table 1
pre_df = pd.read_csv(os.path.join("../data", "collected_macaques", "preprocessing_table.tab"), sep = '\t')
pre_df = pre_df.append(pd.read_csv(os.path.join("../data", "collected_PRJEB26509_26_10_2021", "preprocessing_table.tab"), sep = '\t'))
pre_df.to_excel(os.path.join("../outputs/", "Supplemental_table_1.xlsx"), index = False)

case2species = dict()
for case in clone_df.case.unique():
    species = clone_df.loc[clone_df.case.eq(case), 'species'].values[0]
    case2species[case] = species
    
    
#median genes used for fig 2
median_genes_df = pd.DataFrame()
for gene in ["V", "D", "J"]:
    nuniqs = clone_df.groupby(by = [gene + "_gene_family", "case"], as_index = False).nunique()
    nuniqs['species'] = nuniqs.case.apply(lambda x: case2species[x])
    medians = nuniqs.groupby(by = ["species", gene +"_gene_family"], as_index = False).median()
    medians['gene_family'] = medians[gene + '_gene_family']
    medians['genes'] = medians[gene + "_gene"]
    medians = medians[['species', 'gene_family', 'genes']]
    median_genes_df = median_genes_df.append(medians)
    
median_genes_df = median_genes_df.fillna(0)
median_genes_df['genes'] = median_genes_df['genes'].apply(lambda x: int(x)) 
median_genes_df = median_genes_df.reset_index(drop = True)

gene_family_order = ['IGHV4', 'IGHV3', 'IGHV5', 'IGHV2', 'IGHV1', 'IGHV7', 'IGHV6' ,'IGHD3', 'IGHD6', 'IGHD2', 'IGHD4',  'IGHD1', 'IGHD5', 'IGHD7', 'IGHJ4-3', 'IGHJ5-4', 'IGHJ5-5', 'IGHJ6-6','IGHJ3-2', 'IGHJ1-1','IGHJ2']
family_order_dict = dict(list([(a[1], a[0]) for a in enumerate(gene_family_order)]))
median_genes_df = pd.pivot(median_genes_df, columns = ['species'], index = ['gene_family'], values = ["genes"])
#sort gene families by expression
median_genes_df['gene_family'] = median_genes_df.index.values
median_genes_df['order'] = median_genes_df.gene_family.apply(lambda x: family_order_dict[x])
median_genes_df = median_genes_df.sort_values(by = "order")
#adjust column names
median_genes_df["Family"] = median_genes_df.index.values
median_genes_df= pd.DataFrame({'Family' : median_genes_df[('Family', '')],
                              'Rhesus' : median_genes_df[('genes', 'Rhesus')],
                              'Cynomolgus' : median_genes_df[('genes', 'Cynomolgus')]})
median_genes_df = median_genes_df[["Family", "Rhesus", "Cynomolgus"]].reset_index(drop = True)
median_genes_df.to_csv(os.path.join(macaque_output_dir, "median_genes.tab"), sep = '\t', index = False)

def pandas2latex(df):
    s = '\\\\\n\hline\n'.join(df.apply(lambda x: " & ".join([str(q).replace("_","\_") for q in x.values]), axis = 1).values)
    s = s + '\\\\\n\hline\n'
    return s

latex_table_string = pandas2latex(median_genes_df)
f = open(os.path.join(macaque_output_dir, "median_genes_latex_table.txt"), "w")
f.write(latex_table_string)

print("Calculating macaque VDJ gene family frequencies per case")
VDJ_freq_per_case_df = VDJ_frequencies(clone_df, "case", "gene_family", extra_columns = ["species", "subgroup"])
VDJ_freq_per_case_df.to_csv(os.path.join(macaque_output_dir, 'VDJ_gene_family_frequency_per_case.tab'), sep = '\t', index = False)




clone_df = pd.read_csv(human_collapsed_file, sep = '\t')
clone_df['library'] = clone_df['case']
for segment in ['V', 'D']:
    clone_df[segment + '_gene_family'] = clone_df[segment + '_gene'].apply(lambda x: x.split('-')[0])
#J genes do not have families, only genes
clone_df['J_gene_family'] = clone_df['J_gene']

print("Calculating human VDJ gene frequencies per library")
VDJ_freq_per_lib_df = VDJ_frequencies(clone_df, "library", "gene", extra_columns = ["species"])
VDJ_freq_per_lib_df.to_csv(os.path.join(human_output_dir, 'VDJ_gene_frequency_per_library.tab'), sep = '\t', index = False)

print("Calculating human VDJ gene family frequencies per library")
VDJ_freq_per_lib_df = VDJ_frequencies(clone_df, "library", "gene_family", extra_columns = ["species"])
VDJ_freq_per_lib_df.to_csv(os.path.join(human_output_dir, 'VDJ_gene_family_frequency_per_library.tab'), sep = '\t', index = False)

print("Done.")