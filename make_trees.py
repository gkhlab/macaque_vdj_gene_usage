import pandas as pd 
import os
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import re
import numpy as np
import subprocess

input_dir = "../data"
cyno_db = os.path.join(input_dir, "kimdb", "Cynomolgus")
rhesus_db = os.path.join(input_dir, "kimdb", "Rhesus")
#IMGT database fastas used here only have allele names in sequence names, not the full IMGT descriptions
human_db = os.path.join(input_dir, "IMGT_IGH_2021-10-11")
output_dir =  "../outputs/trees"
mafft_bin = "/usr/local/bin/fftnsi"
fasttree_bin = "/home/mchernys/software/FastTreeDbl"

#load kimdb macaque and IMGT human sequences
db_seqs_dict = dict()
name2name = {'Rhesus' : 'mulatta', 'Cynomolgus' : 'fascicularis'}
for gene in ['V', 'D', 'J']:
    for species in ['Rhesus', 'Cynomolgus']:
        db_seqs_dict[species + "_" + gene] = list(SeqIO.parse(os.path.join(input_dir, 'kimdb', species, "Macaca-" + name2name[species] + "_Ig_Heavy_" + gene + "_1-0.fasta"), 'fasta'))
    db_seqs_dict["Homo_Sapiens_" + gene] = list(SeqIO.parse(os.path.join(human_db, gene + ".fasta"), 'fasta'))

for k in db_seqs_dict.keys():
    for i in range(len(db_seqs_dict[k])):
        r = db_seqs_dict[k][i]
        r.seq = r.seq.upper()
        db_seqs_dict[k][i] = r

#write combined VDJ.fasta files
gene = "V"
for gene in ["V", "D", "J"]:
    records = []
    for d in [("Homo_Sapiens", human_db), ("Rhesus", rhesus_db), ("Cynomolgus", cyno_db)]:
        temp_records = [rec for rec in SeqIO.parse(os.path.join(d[1], gene + ".fasta"), "fasta")]
        for i in range(len(temp_records)):
            temp_records[i].id = temp_records[i].id + "_" + d[0]
            temp_records[i].description = ""
        records = records + temp_records
    SeqIO.write(records, os.path.join(output_dir, gene + ".fasta") , "fasta")
    
    if gene == "V":
        VH4_records = [r for r in records if "V4" in str(r.id)]
        SeqIO.write(VH4_records, os.path.join(output_dir, "V4.fasta") , "fasta")

#create a metadata file which indicates what species allelels can be found in 
species2animal = {'Homo_Sapiens' : "Homo_Sapiens", "Rhesus" : "Macaque", "Cynomolgus" : "Macaque"}
allele_metadata_df = pd.DataFrame()
for gene in ["V", "D", "J"]:
    for species in ["Homo_Sapiens", "Rhesus", "Cynomolgus"]:
        names = [record.id for record in db_seqs_dict[species + '_' + gene]]
        seqs = [str(record.seq) for record in db_seqs_dict[species + '_' + gene]]
        temp_df = pd.DataFrame({"seq_id" : names, "Sequence": seqs})
        temp_df['seq_id'] = temp_df.seq_id.apply(lambda x: x + "_" + species)
        temp_df['Species'] = species
        temp_df['Animal'] = species2animal[species]
        allele_metadata_df = allele_metadata_df.append(temp_df).reset_index(drop = True)
allele_metadata_df["gene_family"] = allele_metadata_df.seq_id.apply(lambda x: re.split("-|_| |\*",x)[0])
allele_metadata_df['allele'] = allele_metadata_df['seq_id'].apply(lambda x: x.split("_")[0])
allele_metadata_df['gene'] = allele_metadata_df['allele'].apply(lambda x: x.split("*")[0])
allele_metadata_df.to_csv(os.path.join(output_dir, "allele_metadata.tab"), sep = '\t', index = False)



#print commands needed to align sequences and generate newick trees 
for gene in ["V", "V4", "J"]:
    temp_df = allele_metadata_df[allele_metadata_df.seq_id.apply(lambda x: "IGH" + gene in x)].reset_index(drop=True)
    sequences = [SeqRecord(seq = Seq(temp_df.loc[i, "Sequence"]), id = temp_df.loc[i, "seq_id"], description = "") for i in temp_df.index.values]
    
    #write combined database files
    SeqIO.write(sequences, os.path.join(output_dir, gene + ".fasta"), "fasta")
    
    #mafft command
    cmd_str = " ".join([mafft_bin,
        "--maxiterate", "1000",
        os.path.join(output_dir, gene + ".fasta"), ">", os.path.join(output_dir, gene + "_aligned.fasta")])
    print(cmd_str)
    subprocess.call(cmd_str, shell = True)
    
    #fasttree command
    cmd_str = " ".join([fasttree_bin, 
                        "-gtr", 
                        "-nt",
                        "-spr", "4",
                        "-mlacc",  "2",
                        "-slownni", 
                        "<", os.path.join(output_dir, gene + "_aligned.fasta"), ">", os.path.join(output_dir, gene + "_aligned_fasttree.newick")])
    print(cmd_str)
    subprocess.call(cmd_str, shell = True)


    

