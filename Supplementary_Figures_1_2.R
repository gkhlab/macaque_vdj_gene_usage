# generate supplementary Figures 1 and 2

library(ggplot2)
library(dplyr)
library(gridExtra)

#set up paths
input_dir = "../data/collected_macaques"
output_dir = "../outputs/"

col_list = c("Chinese" = 'lightskyblue',
             "Indian" = 'mediumpurple',
             "Indonesian" = 'yellow2',
             "Mauritius" = 'tan3', 
             "Rhesus" = 'lightslateblue',
             "Cynomolgus" = 'tan1',
             "leader" = "dimgrey",
             "5' UTR" = "white")

gene_expression = read.table(paste(input_dir, paste("VDJ_gene_frequency_per_library.tab", sep = ""), sep = "/"), header = TRUE)

title_size = 40
for(species in c("Rhesus", "Cynomolgus")){
    combined_df = data.frame()
    combined_grouped_df = data.frame()   
    for (gene in c("V", "D", "J")){

        df = gene_expression[grepl(paste("IGH", gene, sep  = ""), gene_expression$gene) & (gene_expression$species == species),]
        v = as.character(df$leader)
        mapper = c('True' = "leader", 'False' = "5' UTR")
        df$leader = as.factor(as.character(mapper[v]))
        df = df[df$species == species,]
        #subset for species
        #get gene order based off of mean frequency
        grouped_df = df %>% group_by(gene)
        grouped_df = grouped_df %>% summarise(mean(frequency))
        grouped_df = grouped_df[order(grouped_df[["mean(frequency)"]]),]

        #apply gene order
        df = df[order(match(df[["gene"]], grouped_df[["gene"]])),]
        df["gene"] = factor(df$gene,levels = unique(df[['gene']]),ordered = TRUE)
        #get annotations
        f = function(x){
            return(length(unique(df[(as.character(df$gene) == x) & (as.character(df$leader)  == "5' UTR"), "library"])))
        }
        grouped_df["UTR_libraries"] = as.character(lapply(grouped_df$gene, f))
        f = function(x){
            return(length(unique(df[(as.character(df$gene) == x) & (as.character(df$leader)  == "leader"), "library"])))
        }
        grouped_df["leader_libraries"] = as.character(lapply(grouped_df$gene, f))
        f = function(x) {
            return(paste("UTR=", x["UTR_libraries"], ",l=", x["leader_libraries"], sep = ""))
        }
        grouped_df["libraries"] = apply(grouped_df, 1, f) 
        grouped_df['leader'] = 'leader'
        df["gene_type"] = gene
        grouped_df["gene_type"] = gene
        combined_df = rbind(combined_df, df)
        combined_grouped_df = rbind(combined_grouped_df, grouped_df)
    }
    text_size = 23
    legend_text_size = 35
    df = combined_df[combined_df$gene_type == "V",]
    grouped_df = combined_grouped_df[combined_grouped_df$gene_type == "V",]
    rownames(df)  = 1:nrow(df)
    rownames(grouped_df)  = 1:nrow(grouped_df)
    N_text_size = 4
    text_pos = max(df$frequency) - .007
    text_pos1 = rep(text_pos, dim(grouped_df)[1])
    ct_category_column = "leader"
    grouped_df["leader"] = "leader"
    # V, D, and J plots done separately because R does not seem to like me appending plots with multiple aes() values into a list. Figure out why? 
    t = ""
    v = ggplot(df, aes(x = gene, y = frequency, fill = get(ct_category_column))) +  theme_bw()+
        geom_boxplot(position = position_dodge(preserve = "single"), width = .6) +
        ggtitle(t) +
        xlab("Gene") + ylab("Frequency") + ylim(0.0, max(df[["frequency"]]))+ 
        geom_text(data = grouped_df, aes(x = gene, y = text_pos1, label = libraries, hjust = "left"), size=N_text_size) +
        theme(text = element_text(size=text_size), 
              plot.title.position = "plot",
              plot.title = element_text(size=title_size),
                  legend.position = c(.7, .15),
                  legend.key.size = unit(3, 'cm'),
                  legend.text = element_text(size = legend_text_size))+                 
        scale_color_manual(breaks = names(col_list), values=as.character(col_list)) + 
        scale_fill_manual(breaks = names(col_list), values=as.character(col_list)) +
        guides(fill=guide_legend(title="")) + coord_flip()
    df2 = combined_df[combined_df$gene_type == "D",]
    grouped_df2 = combined_grouped_df[combined_grouped_df$gene_type == "D",]
    rownames(df2)  = 1:nrow(df2)
    rownames(grouped_df2)  = 1:nrow(grouped_df2)
    text_pos = max(df2$frequency) -.007
    text_pos2 = rep(text_pos, dim(grouped_df2)[1])
    grouped_df2["leader"] = "leader"
    d = ggplot(df2, aes(x = gene, y = frequency, fill =  get(ct_category_column))) +  theme_bw()+
        geom_boxplot(position = position_dodge(preserve = "single"), width = .6) +
        ggtitle(t) +
        xlab("Gene") + ylab("Frequency") + ylim(0.0, max(df2[["frequency"]]))+ 
        geom_text(data = grouped_df2, aes(x = gene, y = text_pos2, label = libraries, hjust = "left"), size=N_text_size) +
        theme(text = element_text(size=text_size), 
              plot.title.position = "plot",
              plot.title = element_text(size=title_size),
              legend.position = 'none')+                 
        scale_color_manual(breaks = names(col_list), values=as.character(col_list)) + 
        scale_fill_manual(breaks = names(col_list), values=as.character(col_list)) +
        guides(fill=guide_legend(title="")) + 
        coord_flip()
    df3 = combined_df[combined_df$gene_type == "J",]
    grouped_df3 = combined_grouped_df[combined_grouped_df$gene_type == "J",]
    rownames(df)  = 1:nrow(df)
    rownames(grouped_df3)  = 1:nrow(grouped_df3)
    text_pos = max(df3$frequency) - .024
    text_pos3 = rep(text_pos, dim(grouped_df3)[1])
    grouped_df3["leader"] = "leader"
    j = ggplot(df3, aes(x = gene, y = frequency, fill =  get(ct_category_column))) +  theme_bw()+
        geom_boxplot(position = position_dodge(preserve = "single"), width = .6) +
        ggtitle(t) +
        xlab("Gene") + ylab("Frequency") + ylim(0.0, max(df3[["frequency"]]))+ 
        geom_text(data = grouped_df3, aes(x = gene, y = text_pos3, label = libraries, hjust = "left"), size=N_text_size) +
        theme(text = element_text(size=text_size), 
              plot.title.position = "plot",
              plot.title = element_text(size=title_size),
              legend.position = 'none')+                 
        scale_color_manual(breaks = names(col_list), values=as.character(col_list)) + 
        scale_fill_manual(breaks = names(col_list), values=as.character(col_list)) +
        guides(fill=guide_legend(title="")) + 
        coord_flip()
    g = list(v, d, j)

    l = rbind(c(1,1,1,1),
              c(1,1,1,1),
              c(1,1,1,1),
              c(1,1,1,1),
              c(1,1,1,1),
              c(1,1,1,1),
              c(2,2,2,2),
              c(2,2,2,2),
              c(2,2,2,2),
              c(3,3,3,3)
              )
    g = grid.arrange(grobs = g,
                 layout_matrix = l)
    ggsave(paste(output_dir, 'expression_boxplots', "leader_vs_5_UTR", paste(species, "_VDJ_gene_expression_boxplots_leader_vs_5_UTR.png", sep = ""), sep = "/"),g, height = 50, width = 20, limitsize=FALSE)   
}