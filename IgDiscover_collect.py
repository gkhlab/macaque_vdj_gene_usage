"""
Collect results from multiple IgDiscover runs in one folder into one file. 
"""
import pandas as pd
import os
import argparse

def add_arguments(parser):
    arg = parser.add_argument
    arg('run_dir', type=str,
        help='Directory containing IgDiscover run folders.')
    arg('outpath', type=str,
        help='Tab delimited file where to put the collected results')
    arg('--file', type=str, default="iteration-01/new_V_germline.tab",
        help='Path to file you want to collect relative to the individual run folders. For example, '
        'collect new_V_germline tabs by writing iteration-01/new_V_germline.tab.')
    arg('--separator', type=str, default="\t",
        help='Field separator for the file you want to collect.')

def collect(args):
    to_collect =  [[sub, os.path.join(args.run_dir, sub, args.file)] for sub in os.listdir(args.run_dir) if os.path.isdir(os.path.join(args.run_dir, sub))]

    #this should probably be optimized
    df = pd.DataFrame()
    f = to_collect[0]
    temp = pd.read_csv(f[1], sep = args.separator)
    temp['case'] = f[0]
    print(f[0])
    temp.to_csv(args.outpath, sep = '\t', mode = 'w', index = False, header = True)
    for i in range(1, len(to_collect)):
        f = to_collect[i]
        print(f[0])
        temp = pd.read_csv(f[1], sep = args.separator)
        temp['case'] = f[0]
        temp.to_csv(args.outpath, sep = '\t', mode = 'a', index = False, header = False)

parser = argparse.ArgumentParser()
add_arguments(parser)
args = parser.parse_args()
collect(args)
