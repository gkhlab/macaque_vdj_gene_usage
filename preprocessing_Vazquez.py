"""
Prepocess IgDiscover iteration-01/filtered.tab.gz results of 45 macaque IgM rep-seq runs from
Vazquez Bernat, N., et al., Rhesus and cynomolgus macaque immunoglobulin heavy-chain genotyping yields comprehensive databases of germline VDJ alleles. Immunity, 2021. 54(2): p. 355-366 e4.
Preprocessing steps:
1. Subset for results with 0 V_SHM and 0 J_SHM
2. Subset for alleles found in IMGT database from October 11th, 2021
3. Subset for results in the IgDiscover genotype files (iteration-01/new_V_germline.tab, iteration-01/expressed_D,tab, iteration-01/expressed_J,tab)
4. Collapse by V allele, J allele, and CDR3_nt
"""
__author__ = "Mark Chernyshev"

import pandas as pd 
import os
import numpy as np
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
#set up paths
input_dir = "../data/collected_macaques"
output_dir = "../data/collected_macaques"
case_column = "case"

print("Reading in collected IgDiscover results... (this will take a few minutes)")
filt_df = pd.read_csv(os.path.join(input_dir, "collected_filtered.tab.gz"), sep ='\t', usecols = ["V_gene", "D_gene", "J_gene", "CDR3_nt", "CDR3_aa", "count", "case", "V_SHM", "J_SHM"])
cases = np.sort(filt_df[case_column].unique())
preprocessing_df = pd.DataFrame()
preprocessing_df[case_column] = np.sort(filt_df[case_column].unique())
preprocessing_df['Total filtered.tab'] = [filt_df[case_column].eq(n).sum() for n in cases]

filt_df = filt_df[(~filt_df.V_gene.isna()) & (~filt_df.D_gene.isna()) & (~filt_df.J_gene.isna())  & (~filt_df.CDR3_nt.isna())]

for gene in ['V', 'D', 'J']:
    filt_df.columns = [n if n != gene + "_gene" else gene + "_allele" for n in filt_df.columns ] 
    filt_df[gene + '_gene'] = filt_df[gene + '_allele'].apply(lambda x: x.split("*")[0])

#add metadata
metadata_df = pd.read_csv(os.path.join("metadata/Vazquez_metadata.tab"), sep = '\t')
preprocessing_df = pd.merge(preprocessing_df, metadata_df, left_on = "case", right_on = "library")
filt_df = pd.merge(filt_df, metadata_df, left_on = "case", right_on = "library")

#load kimdb macaque and IMGT human sequences
db_seqs_dict = dict()
name2name = {'rhesus' : 'mulatta', 'cyno' : 'fascicularis'}
for gene in ['V', 'D', 'J']:
    db_seqs_dict["homo_sapiens_" + gene] = list(SeqIO.parse(os.path.join("../data/", "IMGT_IGH_2021_JAN_DB/", gene + ".fasta"), 'fasta'))
    for species in ['cyno', 'rhesus']:
        db_seqs_dict[species + "_" + gene] = list(SeqIO.parse(os.path.join("../data/", 'kimdb', species, "Macaca-" + name2name[species] + "_Ig_Heavy_" + gene + "_1-0.fasta"), 'fasta'))

print("Filtering...")
#no SHM 
filt_df = filt_df[filt_df.V_SHM.eq(0.0) & filt_df.J_SHM.eq(0.0)]
preprocessing_df['0 SHM'] = [filt_df[case_column + "_x"].eq(n).sum() for n in cases]

#drop rows containing alleles not in database
subgroup2species = {'Chinese' : 'rhesus', 'Indian' : 'rhesus', 'Indonesian' : 'cyno', 'Mauritius' : 'cyno', "Homo Sapiens" : "homo_sapiens"}
del_inds = []
for subgroup, df in filt_df.groupby(by = 'subgroup'):
    species = subgroup2species[subgroup]
    for gene in ['V', 'D', 'J']:
        allele_names = [record.id for record in db_seqs_dict[species + "_" + gene]]
        del_inds = np.append(del_inds, df[(~df[gene + '_allele'].isin(allele_names)) ].index.values)

filt_df = filt_df.drop(del_inds)
filt_df = filt_df.reset_index(drop = True)
preprocessing_df['Database only'] = [filt_df[case_column + "_x"].eq(n).sum() for n in cases]


#exclude things that are not in IgDiscover calls
new_V = pd.read_csv(os.path.join(input_dir, 'collected_new_V_germline.tab'), sep = '\t')
exp_D = pd.read_csv(os.path.join(input_dir, 'collected_expressed_D.tab'), sep = '\t')
exp_J = pd.read_csv(os.path.join(input_dir, 'collected_expressed_J.tab'), sep = '\t')
del_inds = []
for case, df, in filt_df.groupby(by = case_column + "_x"):
    V_names = new_V.loc[new_V.case.eq(case), 'name']
    D_names = exp_D.loc[exp_D.case.eq(case), 'gene']
    J_names = exp_J.loc[exp_J.case.eq(case), 'gene']
    del_inds = del_inds + list(df[(~df.V_allele.isin(V_names)) | (~df.D_allele.isin(D_names)) | (~df.J_allele.isin(J_names)) ].index.values)
filt_df = filt_df.drop(del_inds)
filt_df = filt_df.reset_index(drop = True)
preprocessing_df['IgDiscover calls'] = [filt_df[case_column + "_x"].eq(n).sum() for n in cases]

print("Collapsing...")
#Collapse by clonotype
#empty D's get filled by NA's during CSV reading
filt_df = filt_df.fillna("").reset_index(drop = True)
filt_df["uncollapsed_count"] = 0
temp_df = filt_df.groupby(by = ['V_allele', 'J_allele', 'CDR3_nt', "case_y"], as_index = False).count()[['V_allele',  'J_allele', 'CDR3_nt', "case_y", 'uncollapsed_count']]
filt_df = filt_df.drop_duplicates(subset = ['V_allele', 'J_allele', 'CDR3_nt', "case_y"]) 
filt_df = pd.merge(filt_df, temp_df, on = ['V_allele', 'J_allele', 'CDR3_nt', "case_y"], how = 'left')
filt_df['uncollapsed_count'] = filt_df['uncollapsed_count_y'] 
filt_df = filt_df.reset_index(drop = True)
preprocessing_df['Clonal collapse'] = [filt_df[case_column + "_x"].eq(n).sum() for n in cases]
preprocessing_df_clean = pd.DataFrame({'Library' : preprocessing_df['case_x'],
                          'Raw Sequences' : [int(q) for q in preprocessing_df['length']],
                          'Filtered.tab' : [int(q) for q in preprocessing_df['Total filtered.tab']],
                          '0 VJ SHM' : [int(q) for q in preprocessing_df['0 SHM']],
                          'Database Only' : [int(q) for q in preprocessing_df['Database only']],
                          'IgDiscover Calls' : [int(q) for q in preprocessing_df['IgDiscover calls']],
                          'Clonal Collapsed' : [int(q) for q in preprocessing_df['Clonal collapse']],
                          'Subgroup' : preprocessing_df['subgroup'].apply(lambda x: x + " Macaque")})

preprocessing_df_clean.to_csv(os.path.join(output_dir, "preprocessing_table.tab"), sep = '\t', index = False)
out = os.path.join(output_dir, "collapsed_no_SHM.tab")
filt_df.to_csv(out, sep = '\t', index = False)
print("Done. Results in " + out)