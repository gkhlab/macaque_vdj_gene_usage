# macaque_VDJ_gene_usage

Scripts to generate results for "VDJ gene usage in IgM repertoires of rhesus and cynomolgus macaques"<br/>
Mark Chernyshev<br/>
8/12/21<br/>

# Dependencies
Python v3.7.4, pandas v1.1.4, scipy v1.4.1, Biopython v1.79, numpy v1.18.0<br/>
R v4.1.2, ComplexHeatmap v2.9.3, msa v1.26.0, ggplot2 v3.3.5, ggtree v3.0.4, ggplot v3.3.5, treeio v1.16.2, ggnewscale v.0.4.5, pals V1.7, gridExtra v2.3, phangorn v2.8.0, circlize v0.4.13, plyr v1.8.6, dplyr v1.0.7, stringr v1.4.0<br/>
MAFFT v7.471<br/>
FastTree v2.1.11<br/>

# Scripts
0. 'IgDiscover_collect.py' collects results from IgDiscover runs
1. 'preprocessing_Vazquez.py' filters and collapses IgDiscover output for macaque libraries 
2. 'preprocessing_Gidoni.py' filters and collapses  IgDiscover output for human libraries 
3. 'VDJ_frequency.py' produces VDJ gene frequencies and Spearman's rank correlation coefficients
4. 'make_trees.py' creates trees for figure 1
5. Figure*.R files generate figures.<br/>
    A. Figure_1.R<br/>
    B. Figure_2.R<br/>
    C. Figures_3_4_5.R<br/>
    D. Figure_6.R<br/>
    E. Supplementary_Figures_1_2.R<br/>




